#define MAX_PACKETSIZE 128 //自定义的缓冲buff长度
char uart_Receive_Buff [ MAX_PACKETSIZE ]; //定义缓冲buff
unsigned int uart_Receive_Index = 0; //收到的字节实际长度
unsigned long preUartTime = 0; //记录读取最好一个字节的时间点
char buf[100];
char wenshi[8]={1,3,0,0,0,2,196,11};  //空气温湿度传感器rtu
char soil[8]={3,3,0,0,0,6,196,42};  //土壤温湿度传感器rtu
unsigned char d_dark[7]={0x3a,0x00,0x04,0x0a,0x00,0x34,0x23};
char buffers[50]; 
String comdata = "";
static const uint32_t GPSBaud = 9600;
#define jd1 50
#define jd2 51
int timeleave = 300;    // 5分钟
int flag = timeleave;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(GPSBaud);
  Serial2.begin(GPSBaud);
  pinMode(jd1,OUTPUT);
  pinMode(jd2,OUTPUT);
}

void Hex2Byte(const char* source, unsigned char* dest, int sourceLen)
{
    short i;
    unsigned char highByte, lowByte;
    for (i = 0; i < sourceLen; i += 2)
    {
        highByte = toupper(source[i]);
        lowByte  = toupper(source[i + 1]);
        if (highByte > 0x39)  
            highByte -= 0x37; //'A'->10 'F'->15
        else
            highByte -= 0x30; //'1'->1 '9'->9
        if (lowByte > 0x39)
            lowByte -= 0x37;
        else
            lowByte -= 0x30;
        dest[i / 2] = (highByte << 4) | lowByte;
    }
}

void split(char *src,const char *separator,char **dest,int *num) {
  /*
    src 源字符串的首地址(buf的地址) 
    separator 指定的分割字符
    dest 接收子字符串的数组
    num 分割后子字符串的个数
  */
     char *pNext;
     int count = 0;
     if (src == NULL || strlen(src) == 0) //如果传入的地址为空或长度为0，直接终止 
        return;
     if (separator == NULL || strlen(separator) == 0) //如未指定分割的字符串，直接终止 
        return;
     pNext = (char *)strtok(src,separator); //必须使用(char *)进行强制类型转换(虽然不写有的编译器中不会出现指针错误)
     while(pNext != NULL) {
          *dest++ = pNext;
          ++count;
         pNext = (char *)strtok(NULL,separator);  //必须使用(char *)进行强制类型转换
    }  
    *num = count;
} 

void loop() {
    Serial.println("1234");
    delay(500);
    while(Serial2.available()){
      comdata += char(Serial2.read());
    }
    Serial.println(comdata);
    if(comdata.indexOf("1-1")!=-1){
      digitalWrite(jd1,HIGH);  
    }
    if(comdata.indexOf("1-0")!=-1){
      digitalWrite(jd1,LOW);  
    }
    if(comdata.indexOf("2-1")!=-1){
      digitalWrite(jd2,HIGH);
    }
    if(comdata.indexOf("2-0")!=-1){
      digitalWrite(jd2,LOW);
      flag=timeleave;
    }     
    //Serial.println("");
    delay(500);
    if(digitalRead(jd2) == HIGH){
        if(flag>0){
          flag--; 
        }else{
          digitalWrite(jd2,LOW);
          flag=timeleave;
        }
    }
    Serial.println(timeleave);
    //delay(500);
    comdata = "";
}
